This repository holds the code for Oscar Legetth and Gustav Svensson's SimpliC compiler.

The  compiler takes SimpliC code (a simple c-like language made up for the sake of [this course](http://cs.lth.se/edan70) and the [compiler course](http://cs.lth.se/edan65)) and produces x86 assembly code and llvm ir code. 

The goal of the project is to compare and evaluate the performance of the two.

To create a compiler type 
```
#!bash
ant jar
```
in your favourite shell (requires [ant](http://ant.apache.org/)). This will create a compiler.jar file which can be run by typing
```
#!bash
java -jar compiler.jar [file] <flags>
```
(requires [java](https://www.java.com/en/download/)). Replace [file] with your desired SimpliC source code file. 
Some examples can be found in the subdirectories of the testfiles directory, look for the .lang files. The -h flag will give all the possible flags for the compiler.

To run all the tests type
```
#!bash
ant test
```
The tests under LLVMCodeGeneration requires [LLVM](http://llvm.org/) version 3.9.0 to run correctly. This should work on Mac Os X and may or may not work on Linux(s).