aspect Visitor {
	/**
	 * Visitor interface for Calc language. Each concrete node type must
	 * have a visit method.
	 */
	public interface Visitor {
		public Object visit(List node, Object data);
		public Object visit(Opt node, Object data);
		public Object visit(Program node, Object data);
		public Object visit(IntType node, Object data);
		public Object visit(FloatType node, Object data);
		public Object visit(Mul node, Object data);
		public Object visit(Div node, Object data);
		public Object visit(Add node, Object data);
		public Object visit(Sub node, Object data);
		public Object visit(Mod node, Object data);
		public Object visit(Numeral node, Object data);
		public Object visit(Id_Var_Use node, Object data);
		public Object visit(Id_Var_Decl node, Object data);
		public Object visit(Var_Decl node, Object data);
		public Object visit(Id_Func_Use node, Object data);
		public Object visit(Id_Func_Decl node, Object data);
		public Object visit(BlockStmt node, Object data);
		public Object visit(Assign node, Object data);
		public Object visit(Function node, Object data);
		public Object visit(Func_Call_Stmt node, Object data);
		public Object visit(Func_Call node, Object data);
		public Object visit(Func_Par node, Object data);
		public Object visit(Return node, Object data);
		public Object visit(While node, Object data);
		public Object visit(If node, Object data);
		public Object visit(IfElse node, Object data);
		public Object visit(LT node, Object data);
		public Object visit(GT node, Object data);
		public Object visit(LET node, Object data);
		public Object visit(GET node, Object data);
		public Object visit(EQ node, Object data);
		public Object visit(NEQ node, Object data);
	}

	public Object ASTNode.accept(Visitor visitor, Object data) {
		throw new Error("Visitor: accept method not available for " + getClass().getName());
	}
	public Object List.accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
	public Object IntType.accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
	public Object FloatType.accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
	public Object Opt.accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
	public Object Program.accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
	public Object Add.accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
	public Object Sub.accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
	public Object Mul.accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
	public Object Div.accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
	public Object Mod.accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
	public Object Numeral.accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
	public Object Var_Decl.accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
	public Object Id_Var_Use.accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
	public Object Id_Var_Decl.accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
	public Object Id_Func_Use.accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
	public Object Id_Func_Decl.accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
	public Object Assign.accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
	public Object IfElse.accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
	public Object If.accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
	public Object While.accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
	public Object Return.accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
	public Object BlockStmt.accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
	public Object Function.accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
	public Object Func_Call_Stmt.accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
	public Object Func_Call.accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
	public Object Func_Par.accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
	public Object LT.accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
	public Object GT.accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
	public Object LET.accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
	public Object GET.accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
	public Object EQ.accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
	public Object NEQ.accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
}