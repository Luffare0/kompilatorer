package lang;

import lang.ast.*;

/**
 * Checks the maximum statement nesting in a SimpliC program.
 */
public class CheckMSN extends TraversingVisitor {
	// client method
	public static int result(ASTNode n) {
		CheckMSN v = new CheckMSN();
		n.accept(v, null);
		return v.msn_max;
	}

	// state variables
	private int msn = 0;
	private int msn_max = 0;

	public Object visit(BlockStmt node, Object data) {
		msn++;
		if (msn_max < msn) {
			msn_max = msn;
		}
		Object o = super.visit(node, data);
		msn--;
		return o;
	}
}
