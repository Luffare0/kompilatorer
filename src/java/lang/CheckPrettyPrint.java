package lang;

import lang.ast.*;

/**
 * Prints a SimpliC program in a pretty way.
 */
public class CheckPrettyPrint extends TraversingVisitor {
	// client method
	public static void result(ASTNode n) {
		CheckPrettyPrint v = new CheckPrettyPrint();
		n.accept(v, null);
	}
}
