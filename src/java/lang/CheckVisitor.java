package lang;

import lang.ast.*;

/**
 * Checks if there are any 'ask user' statements in a Calc program.
 */
public class CheckVisitor extends TraversingVisitor {
	// client method
	public static boolean result(ASTNode n) {
		CheckVisitor v = new CheckVisitor();
		n.accept(v, null);
		return v.isMul;
	}

	// state variables
	private boolean isMul = false;

	public Object visit(Mul node, Object data) {
		isMul = true;
		return data;
	}
}
