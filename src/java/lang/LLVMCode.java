package lang;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import beaver.Parser.Exception;
import lang.ast.LangParser;
import lang.ast.LangScanner;
import lang.ast.Program;

/**
 * This class is used by the ant file to generate the compiler.jar file
 */

public class LLVMCode {

	public static Object DrAST_root_node; // Enable debugging with DrAST

	public static void main(String[] args) {
		try {
			if (args.length < 1) {
				System.err.println("You must specify a source file on the command line and a desired output file");
				printHelp();
				System.exit(1);
				return;
			}

			if (args[0].equals("-h")) {
				printHelp();
				System.exit(0);
			}

			String inFile = args[0];
			LangScanner scanner = new LangScanner(new FileReader(inFile));
			LangParser parser = new LangParser();
			Program program = (Program) parser.parse(scanner);
			DrAST_root_node = program; // Enable debugging with DrAST
			String outFile = "a.out";
			boolean x86 = false;
			boolean noopt = false;
			boolean x86opt = false;
			boolean keepFiles = false;
			for (int i = 1; i < args.length; ++i) {
				String arg = args[i];
				switch (arg) {
				case "-a":
					keepFiles = true;
					break;
				case "-o":
					if (i + 1 >= args.length) {
						System.err.println("No outfile specified");
						System.exit(1);
					}
					outFile = args[++i];
					break;
				case "--x86":
					x86 = true;
					break;
				case "--x86opt":
					x86opt = true;
					break;
				case "--noopt":
					noopt = true;
					break;
				}
			}
			if (x86) {
				// Generate Assembly file
				File assemblyFile = getFileReplaceExtension(outFile, ".s");
				PrintStream out = new PrintStream(new FileOutputStream(assemblyFile));
				program.genCode(out, "_start");
				out.close();

				// Generate object file
				File objectFile = getFileReplaceExtension(outFile, ".o");
				ArrayList<String> cmdAs = new ArrayList<String>();
				cmdAs.add("as");
				cmdAs.add(assemblyFile.getAbsolutePath());
				cmdAs.add("-o");
				cmdAs.add(objectFile.getAbsolutePath());
				execute(cmdAs);

				// Link object file and generate executable file
				ArrayList<String> cmdLd = new ArrayList<String>();
				cmdLd.add("ld");
				cmdLd.add(objectFile.getAbsolutePath());
				cmdLd.add("-o");
				cmdLd.add(outFile);
				execute(cmdLd);

				if (!keepFiles) {
					rmFile(assemblyFile.getAbsolutePath());
					rmFile(objectFile.getAbsolutePath());
				}

			} else if (x86opt) {
				// Generate Assembly file
				File assemblyFile = getFileReplaceExtension(outFile, ".s");
				PrintStream out = new PrintStream(assemblyFile);
				program.genCode(out, "main");
				out.close();

				// Compile and optimize with gcc
				ArrayList<String> cmdGcc = new ArrayList<String>();
				cmdGcc.add("gcc");
				cmdGcc.add("-O3");
				cmdGcc.add(assemblyFile.getAbsolutePath());
				cmdGcc.add("-o");
				cmdGcc.add(outFile);
				execute(cmdGcc);

			} else if (noopt) {
				File llvmCodeFile = getFileReplaceExtension(outFile, ".ll");
				PrintStream llvmCodeOut = new PrintStream(llvmCodeFile);
				program.genLLVMCode(llvmCodeOut);
				llvmCodeOut.close();

			} else {
				File llvmCodeFile = getFileReplaceExtension(outFile, ".ll");
				PrintStream llvmCodeOut = new PrintStream(llvmCodeFile);
				program.genLLVMCode(llvmCodeOut);
				llvmCodeOut.close();
				String errors = program.errors().toString();
				if (!errors.equals("[]")) {
					System.err.println("Compile errors. Aborting.");
					System.err.println(errors);
					System.exit(1);
				}

				// generate optimized LLVM IR code
				File llvmCodeOpt = getFileReplaceExtension(outFile, ".opt");
				ArrayList<String> cmdOpt = new ArrayList<String>();
				cmdOpt.add("opt");
				cmdOpt.add("-O3");
				cmdOpt.add("-S");
				cmdOpt.add(llvmCodeFile.getAbsolutePath());
				cmdOpt.add("-o");
				cmdOpt.add(llvmCodeOpt.getAbsolutePath());
				execute(cmdOpt);

				// generate assemblyfile from optimized LLVM IR code
				File assemblyFile = getFileReplaceExtension(outFile, "opt.s");
				ArrayList<String> cmdLlc = new ArrayList<String>();
				cmdLlc.add("llc");
				cmdLlc.add(llvmCodeOpt.getAbsolutePath());
				cmdLlc.add("-o");
				cmdLlc.add(assemblyFile.getAbsolutePath());
				execute(cmdLlc);

				// Generate object file from optimized assemblycode
				File objectFile = getFileReplaceExtension(outFile, "opt.o");
				ArrayList<String> cmdAs = new ArrayList<String>();
				cmdAs.add("as");
				cmdAs.add("-q"); // use clang.
				cmdAs.add(assemblyFile.getAbsolutePath());
				cmdAs.add("-o");
				cmdAs.add(objectFile.getAbsolutePath());
				execute(cmdAs);

				// Link object file using gcc
				ArrayList<String> cmdGcc = new ArrayList<String>();
				cmdGcc.add("gcc");
				cmdGcc.add(objectFile.getAbsolutePath());
				cmdGcc.add("-O3");
				cmdGcc.add("-o");
				cmdGcc.add(outFile);
				execute(cmdGcc);

				if (!keepFiles) {
					// clean up intermediate files
					rmFile(llvmCodeFile.getAbsolutePath());
					rmFile(llvmCodeOpt.getAbsolutePath());
					rmFile(assemblyFile.getAbsolutePath());
					rmFile(objectFile.getAbsolutePath());
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println("File not found!");
			System.exit(1);
		} catch (IOException e) {
			e.printStackTrace(System.err);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static File getFileReplaceExtension(String filename, String extension) {
		int lastPeriod = filename.lastIndexOf('.');
		String simpleName;
		if (lastPeriod != -1) {
			simpleName = filename.substring(0, filename.lastIndexOf('.'));
		} else {
			simpleName = filename;
		}
		return new File(simpleName + extension);
	}

	private static void rmFile(String file) throws IOException {
		ArrayList<String> cmd = new ArrayList<String>();
		cmd.add("rm");
		cmd.add(file);
		ProcessBuilder pb = new ProcessBuilder(cmd);
		Process process = pb.start();
		process.getOutputStream().close();
		try {
			process.waitFor();
		} catch (InterruptedException e) {
			System.err.println("Process was interrupted.");
			e.printStackTrace();
		}
	}

	private static void execute(List<String> cmd) throws IOException {
		ProcessBuilder pb = new ProcessBuilder(cmd);
		Process process = pb.start();
		process.getOutputStream().close();
		try {
			process.waitFor();
		} catch (InterruptedException e) {
			System.err.println("Process was interrupted.");
			e.printStackTrace();
		}
		String stderr = inputStreamToString(process.getErrorStream());
		if (!stderr.equals("")) {
			System.err.println(stderr);
		}
		if (process.exitValue() != 0) {
			System.err.println("Exit code was not zero (error occured) when running command '" + cmd.get(0) + "'");
		}
	}

	private static String inputStreamToString(InputStream is) {
		StringBuilder sb = new StringBuilder();
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			String line;
			while ((line = br.readLine()) != null) {
				sb.append(line).append("\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
		return sb.toString();
	}

	private static void printUsage() {
		System.err.println("Usage: Compiler FILE_IN <flags>");
		System.err.println("  where FILE_IN is the file to be compiled");
	}

	private static void printHelp() {
		System.out.println("A compiler for SimpliC.");
		printUsage();
		System.out.println("Flags:");
		System.out.println("  -a		don't remove intermediate files such as assembly code and object files");
		System.out.println("  -h		display this help message");
		System.out.println("  -o [FILE]	desired name of the output file");
		System.out.println("  --nopt	generate horrible unoptimized llvm ir code (run using lli)");
		System.out.println("  --x86		generate horrible unoptimized x86 assembly code instead");
		System.out.println(
				"  --x86opt	generate very nice optimized x86 assembly code instead (optimized with gcc -O3)");
	}
}
