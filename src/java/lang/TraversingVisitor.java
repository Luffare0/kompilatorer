package lang;

import lang.ast.*;

/**
 * Traverses each node, passing the data to the children. Returns the data
 * unchanged. Overriding methods may change the data passed and the data
 * returned.
 */
public abstract class TraversingVisitor implements lang.ast.Visitor {

	private Object visitChildren(ASTNode node, Object data) {
		for (int i = 0; i < node.getNumChild(); ++i) {
			node.getChild(i).accept(this, data);
		}
		return data;
	}

	public Object visit(List node, Object data) {
		return visitChildren(node, data);
	}

	public Object visit(IntType node, Object data) {
		return visitChildren(node, data);
	}

	public Object visit(FloatType node, Object data) {
		return visitChildren(node, data);
	}

	public Object visit(Opt node, Object data) {
		return visitChildren(node, data);
	}

	public Object visit(Program node, Object data) {
		return visitChildren(node, data);
	}

	public Object visit(Mul node, Object data) {
		return visitChildren(node, data);
	}

	public Object visit(Div node, Object data) {
		return visitChildren(node, data);
	}

	public Object visit(Add node, Object data) {
		return visitChildren(node, data);
	}

	public Object visit(Sub node, Object data) {
		return visitChildren(node, data);
	}

	public Object visit(Mod node, Object data) {
		return visitChildren(node, data);
	}

	public Object visit(Numeral node, Object data) {
		return visitChildren(node, data);
	}

	public Object visit(Id_Var_Use node, Object data) {
		return visitChildren(node, data);
	}

	public Object visit(Id_Var_Decl node, Object data) {
		return visitChildren(node, data);
	}

	public Object visit(Var_Decl node, Object data) {
		return visitChildren(node, data);
	}

	public Object visit(Id_Func_Use node, Object data) {
		return visitChildren(node, data);
	}

	public Object visit(Id_Func_Decl node, Object data) {
		return visitChildren(node, data);
	}

	public Object visit(BlockStmt node, Object data) {
		return visitChildren(node, data);
	}

	public Object visit(Assign node, Object data) {
		return visitChildren(node, data);
	}

	public Object visit(Function node, Object data) {
		return visitChildren(node, data);
	}

	public Object visit(Return node, Object data) {
		return visitChildren(node, data);
	}

	public Object visit(While node, Object data) {
		return visitChildren(node, data);
	}

	public Object visit(If node, Object data) {
		return visitChildren(node, data);
	}

	public Object visit(IfElse node, Object data) {
		return visitChildren(node, data);
	}

	public Object visit(Func_Par node, Object data) {
		return visitChildren(node, data);
	}

	public Object visit(Func_Call_Stmt node, Object data) {
		return visitChildren(node, data);
	}

	public Object visit(Func_Call node, Object data) {
		return visitChildren(node, data);
	}

	public Object visit(LT node, Object data) {
		return visitChildren(node, data);
	}

	public Object visit(GT node, Object data) {
		return visitChildren(node, data);
	}

	public Object visit(LET node, Object data) {
		return visitChildren(node, data);
	}

	public Object visit(GET node, Object data) {
		return visitChildren(node, data);
	}

	public Object visit(EQ node, Object data) {
		return visitChildren(node, data);
	}

	public Object visit(NEQ node, Object data) {
		return visitChildren(node, data);
	}

}
