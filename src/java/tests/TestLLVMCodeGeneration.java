package tests;

import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import lang.ast.Program;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class TestLLVMCodeGeneration extends AbstractParameterizedTest {

	private static final String TEST_DIR = "testfiles/llvm";

	/**
	 * Construct a new code llvm generation test
	 * 
	 * @param filename
	 *            filename of test input file
	 */
	public TestLLVMCodeGeneration(String filename) {
		super(TEST_DIR, filename);
	}

	/**
	 * Run the code generation test
	 */
	@Test
	public void runTest() throws IOException, Exception {
		Program program = (Program) parse(inFile);
		assertEquals("[]", program.errors().toString());

		String system = System.getProperty("os.name");

		// System.out.println(system);
		String version = System.getProperty("os.version");
		// System.out.println(version);
		String arch = System.getProperty("os.arch");
		// System.out.println(arch);

		// generate unoptimized LLVM IR code
		File llvmFile = getFileReplaceExtension(inFile, ".ll");
		PrintStream out = new PrintStream(new FileOutputStream(llvmFile));
		program.genLLVMCode(out);
		out.close();

		// generate optimized LLVM IR code
		File llvmFileOpt = getFileReplaceExtension(inFile, "opt.ll");
		ArrayList<String> cmdOpt = new ArrayList<String>();
		cmdOpt.add("opt");
		cmdOpt.add("-S");
		cmdOpt.add(llvmFile.getAbsolutePath());
		cmdOpt.add("-o");
		cmdOpt.add(llvmFileOpt.getAbsolutePath());
		execute(cmdOpt);

		// generate assemblyfile from unoptimized LLVM IR code
		File assemblyFile = getFileReplaceExtension(inFile, ".s");
		ArrayList<String> cmdLlc = new ArrayList<String>();
		cmdLlc.add("llc");
		cmdLlc.add(llvmFile.getAbsolutePath());
		cmdLlc.add("-o");
		cmdLlc.add(assemblyFile.getAbsolutePath());
		execute(cmdLlc);

		// generate assemblyfile from optimized LLVM IR code
		File assemblyFileOpt = getFileReplaceExtension(inFile, "opt.s");
		ArrayList<String> cmdLlcOpt = new ArrayList<String>();
		cmdLlcOpt.add("llc");
		cmdLlcOpt.add(llvmFileOpt.getAbsolutePath());
		cmdLlcOpt.add("-o");
		cmdLlcOpt.add(assemblyFileOpt.getAbsolutePath());
		execute(cmdLlcOpt);

		// Generate object file from unoptimized assemblycode
		File objectFile = getFileReplaceExtension(inFile, ".o");
		ArrayList<String> cmdAs = new ArrayList<String>();
		cmdAs.add("as");
		cmdAs.add("-q"); // use clang.
		cmdAs.add(assemblyFile.getAbsolutePath());
		cmdAs.add("-o");
		cmdAs.add(objectFile.getAbsolutePath());
		execute(cmdAs);
		
		rmFile(assemblyFile.getAbsolutePath());

		// Generate object file from optimized assemblycode
		File objectFileOpt = getFileReplaceExtension(inFile, "opt.o");
		ArrayList<String> cmdAsOpt = new ArrayList<String>();
		cmdAsOpt.add("as");
		cmdAsOpt.add("-q"); // use clang.
		cmdAsOpt.add(assemblyFileOpt.getAbsolutePath());
		cmdAsOpt.add("-o");
		cmdAsOpt.add(objectFileOpt.getAbsolutePath());
		execute(cmdAsOpt);
		
		rmFile(assemblyFileOpt.getAbsolutePath());

		// Link unoptimized object file and generate unoptimized executable file
		File execFile = null;
		ArrayList<String> cmdLd = new ArrayList<String>();
		if (system.equals("Mac OS X")) {
			execFile = getFileReplaceExtension(inFile, "");
			cmdLd.add("ld");
			cmdLd.add(objectFile.getAbsolutePath());
			cmdLd.add("-lSystem");
			cmdLd.add("-o");
			cmdLd.add(execFile.getAbsolutePath());
			cmdLd.add("-arch");
			cmdLd.add(arch);
			cmdLd.add("-macosx_version_min");
			cmdLd.add(version);
			execute(cmdLd);
		} else if (system.equals("Linux")) {
			execFile = getFileReplaceExtension(inFile, ".elf");

			// cmdLd.add("ld");
			// cmdLd.add(objectFile.getAbsolutePath());
			// cmdLd.add("-lc");
			// cmdLd.add("-o");
			// cmdLd.add(execFile.getAbsolutePath());
			// cmdLd.add("-A " + arch);
			// cmdLd.add("-e main");

			// Since everyone on the internet says "don't use ld it's bad" and
			// since we can't solve the problem ld has with entrypoints, we
			// won't use ld.
			// Link using gcc instead.
			cmdLd.add("gcc");
			cmdLd.add(objectFile.getAbsolutePath());
			cmdLd.add("-O0");
			cmdLd.add("-o");
			cmdLd.add(execFile.getAbsolutePath());
			execute(cmdLd);

		}
		
		rmFile(objectFile.getAbsolutePath());

		// Run the unoptimized executable file

		long start = System.currentTimeMillis();
		// for(int i = 0; i < 100; i++){
		if (execFile != null) {
			String output = execute(Arrays.asList(execFile.getAbsolutePath()));
			compareOutput(output, outFile, expectedFile);
		}
		// }
		long end = System.currentTimeMillis();
		// System.out.println("Execution time of unoptimized code: " + (end -
		// start));

		// Link optimized object file and generate optimized executable file
		File execFileOpt = null;
		ArrayList<String> cmdLdOpt = new ArrayList<String>();
		if (system.equals("Mac OS X")) {
			execFileOpt = getFileReplaceExtension(inFile, "opt");
			cmdLdOpt.add("ld");
			cmdLdOpt.add(objectFileOpt.getAbsolutePath());
			cmdLdOpt.add("-lSystem");
			cmdLdOpt.add("-o");
			cmdLdOpt.add(execFileOpt.getAbsolutePath());
			cmdLdOpt.add("-arch");
			cmdLdOpt.add(arch);
			cmdLdOpt.add("-macosx_version_min");
			cmdLdOpt.add(version);
			execute(cmdLdOpt);
		} else if (system.equals("Linux")) {
			execFileOpt = getFileReplaceExtension(inFile, "opt.elf");
			// cmdLdOpt.add("ld");
			// cmdLdOpt.add(objectFile.getAbsolutePath());
			// cmdLdOpt.add("-lc");
			// cmdLdOpt.add("-o");
			// cmdLdOpt.add(execFile.getAbsolutePath());
			// cmdLdOpt.add("-A " + arch);
			// cmdLdOpt.add("-e main");
			cmdLdOpt.add("gcc");
			cmdLdOpt.add(objectFileOpt.getAbsolutePath());
			cmdLdOpt.add("-O3");
			cmdLdOpt.add("-o");
			cmdLdOpt.add(execFileOpt.getAbsolutePath());
			execute(cmdLdOpt);
		}
		
		// clean up intermediate files
		
		
		rmFile(objectFileOpt.getAbsolutePath());
		
		// Run the optimized executable file
		start = System.currentTimeMillis();
		// for(int i = 0; i < 100; i++){
		if (execFileOpt != null) {
			String outputOpt = execute(Arrays.asList(execFileOpt.getAbsolutePath()));
			compareOutput(outputOpt, outFile, expectedFile);
		}
		// }
		end = System.currentTimeMillis();
		// System.out.println("Execution time of optimized code: " + (end -
		// start));
	}

	private String execute(List<String> cmd) throws IOException, InterruptedException {
		ProcessBuilder pb = new ProcessBuilder(cmd);
		Process process = pb.start();
		process.getOutputStream().close();
		process.waitFor();

		String standardError = inputStreamToString(process.getErrorStream());
		assertEquals("Standard error was not empty when running command '" + cmd.get(0) + "'", "", standardError);
		assertEquals("Exit code was not zero (error occured) when running command '" + cmd.get(0) + "'", 0,
				process.exitValue());

		return inputStreamToString(process.getInputStream());
	}
	
	private static void rmFile(String file) throws IOException {
		ArrayList<String> cmd = new ArrayList<String>();
		cmd.add("rm");
		cmd.add(file);
		ProcessBuilder pb = new ProcessBuilder(cmd);
		Process process = pb.start();
		process.getOutputStream().close();
		try {
			process.waitFor();
		} catch (InterruptedException e) {
			System.err.println("Process was interrupted.");
			e.printStackTrace();
		}
	}

	private String inputStreamToString(InputStream is) {
		StringBuilder sb = new StringBuilder();
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			String line;
			while ((line = br.readLine()) != null) {
				sb.append(line).append("\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
		return sb.toString();
	}

	@SuppressWarnings("javadoc")
	@Parameters(name = "{0}")
	public static Iterable<Object[]> getTests() {
		return getTestParameters(TEST_DIR);
	}

}
