package lang.ast; // The generated scanner will belong to the package lang.ast

import lang.ast.LangParser.Terminals; // The terminals are implicitly defined in the parser
import lang.ast.LangParser.SyntaxError;

%%

// define the signature for the generated scanner
%public
%final
%class LangScanner
%extends beaver.Scanner

// the interface between the scanner and the parser is the nextToken() method
%type beaver.Symbol 
%function nextToken 

// store line and column information in the tokens
%line
%column

// this code will be inlined in the body of the generated scanner class
%{
  private beaver.Symbol sym(short id) {
    return new beaver.Symbol(id, yyline + 1, yycolumn + 1, yylength(), yytext());
  }
%}

// macros
WhiteSpace = [ ] | \t | \f | \n | \r
Comment = [/][/][^\n\r]*
BOOLEAN_LITERAL = "true" | "false"
ID = [a-zA-Z][a-zA-Z0-9_]*
INT_NUMERAL = [0-9]+
FLOAT_NUMERAL = [0-9]*[.][0-9]+

%%

// discard whitespace information
{WhiteSpace}  { }
// ignore comments
{Comment} { }

// token definitions

"boolean" 			{ return sym(Terminals.BOOL); }
"void"				{ return sym(Terminals.VOID); }
"int"				{ return sym(Terminals.INT); }
"float"				{ return sym(Terminals.FLOAT); }
"if"				{ return sym(Terminals.IF); }
"while"				{ return sym(Terminals.WHILE); }
"else"				{ return sym(Terminals.ELSE); }
"return"			{ return sym(Terminals.RETURN); }
"("					{ return sym(Terminals.LPAREN); }
")"					{ return sym(Terminals.RPAREN); }
"{"					{ return sym(Terminals.LCURLY); }
"}"					{ return sym(Terminals.RCURLY); }
","					{ return sym(Terminals.COMMA); }
";"					{ return sym(Terminals.SEMICOLON); }
"="					{ return sym(Terminals.ASSIGN); }
"*"					{ return sym(Terminals.MUL); }
"/"					{ return sym(Terminals.DIV); }
"+"					{ return sym(Terminals.ADD); }
"-"					{ return sym(Terminals.SUB); }
"%"					{ return sym(Terminals.MOD); }
"<"					{ return sym(Terminals.LT); }
">"					{ return sym(Terminals.GT); }
"<="				{ return sym(Terminals.LET); }
">="				{ return sym(Terminals.GET); }
"=="				{ return sym(Terminals.EQ); }
"!="				{ return sym(Terminals.NEQ); }
{BOOLEAN_LITERAL} 	{ return sym(Terminals.BOOLEAN_LITERAL); }
{ID}				{ return sym(Terminals.ID); }
{INT_NUMERAL}		{ return sym(Terminals.INT_NUMERAL); }
{FLOAT_NUMERAL}		{ return sym(Terminals.FLOAT_NUMERAL); }
<<EOF>>				{ return sym(Terminals.EOF); }

/* error fallback */
[^]					{ throw new SyntaxError("Illegal character <"+yytext()+">"); }
