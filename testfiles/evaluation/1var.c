#include <stdio.h>

int main() {
  int a = 0;

  int fib(int n) {
    a = a + 1;
    if (n < 2) {
      return 1;
    }
    return fib(n - 1) + fib(n - 2);
  }

  printf("%d\n", fib(39));
  return 0;
}
