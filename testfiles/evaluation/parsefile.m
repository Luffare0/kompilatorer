function [real, user] = parsefile (file)
fi = fopen(file, 'r');
tmp = fscanf(fi, 'real:%f\tuser:%f\n', [2, 20])';
real = tmp(1:20 ,1)';
user = tmp(1:20, 2)';
fclose(fi);
