function [user_c] = plottimes ()
[real1, user1] = parsefile('timeresultsnew_1.txt');
[real2, user2] = parsefile('timeresultsnew_2.txt');
[real3, user3] = parsefile('timeresultsnew_3.txt');
[real4, user4] = parsefile('timeresultsnew_4.txt');
[real5, user5] = parsefile('timeresultsnew_5.txt');

user = [ user1; user2; user3; user4; user5 ];

user_t = repmat(tinv([0.025  0.975], length(user)-1)', 1, 20);
user_tmp = std(user)/sqrt(length(user))';
user_c = [mean(user); mean(user)] + user_t .* [user_tmp; user_tmp];

real = [ real1; real2; real3; real4; real5 ];

real_t = repmat(tinv([0.025  0.975], length(real)-1)', 1, 20);
real_tmp = std(real)/sqrt(length(real))';
real_c = [mean(real); mean(real)] + real_t .* [real_tmp; real_tmp];

x_axis = 1:5;
figure(1)
hold off
%plot(x_axis, user_c(:,1:5), 'r+');
h2 = plot(x_axis, user_c(:,6:10), 'b+');
hold on
h3 = plot(x_axis, user_c(:,11:15), 'r+');
h4 = plot(x_axis, user_c(:,16:20), 'k+');

title('Results for program fibonacci(39)');
xlabel('Number of variables');
ylabel('Execution time (s)');
legend( [h2(1) h3(1) h4(1)], {'optimized llvm', 'unoptimized asm', 'optimized c'}, 'Location', 'northwest');
%legend( [h2(1) h3(1) ], {'optimized llvm', 'unoptimized asm' }, 'Location', 'northwest');

%plot(x_axis, real_c(:,1:5), 'rx')
plot(x_axis, real_c(:,6:10), 'bx')
plot(x_axis, real_c(:,11:15), 'rx')
plot(x_axis, real_c(:,16:20), 'kx')
axis([.9 5.1 0 inf]);
set(gca,'xtick', 1:5);
